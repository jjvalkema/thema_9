---
title: "Birds' bones and living habits"
author: "Jimjim Valkema"
date: "9/10/2019"
output:
  pdf_document: default
  html_document: default
---
```{r, echo=F}

df <- read.csv("bird.csv")
# summary(df)

df <- read.csv("bird.csv")
# remove rows with NA's
df <- df[complete.cases(df),]
```   



```{r, echo=F}
# ggplot(data = min_max_norm, aes(x=huml,y=ulnal,colour = factor(type))) + geom_point() + ggtitle( "correlation between Humerus and Ulna")
# ggplot(data = min_max_norm, aes(x=feml,y=tibl,colour = factor(type))) + geom_point() + ggtitle( "correlation between Femur and Tibiotarsus")
# ggplot(data = min_max_norm, aes(x=huml,y=tibl,colour = factor(type))) + geom_point() + ggtitle( "correlation between Humerus and Tibiotarsus")

# print("lenght vs diameter")
# cor(x=min_max_norm$huml, y= min_max_norm$humw)
# cor(x=min_max_norm$ulnal, y= min_max_norm$ulnaw)
# cor(x=min_max_norm$feml, y= min_max_norm$femw)
# cor(x=min_max_norm$tibl, y= min_max_norm$tibw)
# cor(x=min_max_norm$tarl, y= min_max_norm$tarw)

# ggplot(data = scaled_norm, aes(x=feml,y=femw,colour = factor(type))) + geom_point() + ggtitle( "correlation between Femur length and width")
# ggplot(data = scaled_norm, aes(x=tarl,y=tarw,colour = factor(type))) + geom_point() + ggtitle( "correlation between Tarsometatarsus length and width")

```   

## Conclusion
The data seems to be reasonably complete apart from a few birds missing some attributes so these birds should be removed from the data. The ranges of different attributes are quite significant so the data needs to be normalised.  
It is found that scaling by the standard deviation would be the most effective method for normalization for a future proof machine learning model.  
The data also benefits from a log2 transformation because it seems to be more normal distributed this way. 
  
The data is also heavily correlated especially the width and length of the bones. However it is still up for debate if combining these values is beneficial for the machine learning process.

The PCA plots show that it would be difficult to identify all birds habitats from this data. However the PCA does show that separating singing birds from swimming birds might be doable.
```{r, echo=F, warning=F, message=FALSE}
library("ggplot2")
library(gridExtra)
library(reshape2)
library(rlist)
library(tidyr)

#scale the data
scale_min_max <- function(x){
  x_min = min(x, na.rm = T)
  x_max = max(x, na.rm = T)
  return ((x - x_min) / (x_max - x_min))
}

logged <- as.data.frame(log2(df[,2:11]))
min_max_norm <- as.data.frame(apply(logged, MARGIN = 2, FUN = scale_min_max))
scaled_norm <- as.data.frame(scale(logged))

logged["type"] <- df["type"]
min_max_norm["type"] <- df["type"]
scaled_norm["type"] <- df["type"]

# boxplot
melted.oc <- melt(df[2:11])
melted.log.norm <- melt(scaled_norm[1:10])
melted.scaled.norm <- melt(min_max_norm[1:10])

ggplot(data = melted.oc, aes(x=variable,y=value)) + geom_boxplot(outlier.colour="black", outlier.shape=16,outlier.size=2, notch=T) + ggtitle("all atributes not scaled")
#ggplot(data = melted.log.norm, aes(x=variable,y=value)) + geom_boxplot(outlier.colour="black", outlier.shape=16,outlier.size=2, notch=T) + ggtitle("all atributes 2log and scaled")
ggplot(data = melted.scaled.norm, aes(x=variable,y=value)) + geom_boxplot(outlier.colour="black", outlier.shape=16,outlier.size=2, notch=T) + ggtitle("all atributes 2log and min and max normalised")

#create histogram
melted_scaled_norm <- gather(scaled_norm[1:10])
melted_df <- gather(df[2:11])
ggplot(data=melted_df, aes(value))  + facet_wrap(~key,scales="free") + geom_density(adjust = 0.5)+ ggtitle( "attributes density")
ggplot(data=melted_scaled_norm, aes(value))  + facet_wrap(~key) + geom_density(adjust = 0.5)+ ggtitle("log transformed attributes density")



```
  
```{r, echo=F}
library("ggcorrplot")
cor.data.scaled <- cor(scaled_norm[,1:10])
ggcorrplot(cor.data.scaled,  ggtheme = ggplot2::theme_gray,hc.order = T,lab = T) + ggtitle( "correlation matrix of the bones lenght and width")


hum <- df$huml * df$humw
ulna <- df$ulnal * df$ulnaw
fem <- df$feml * df$femw
tib <- df$tibl * df$tibw
tar <- df$tarl * df$tarw

bone.volumes <- data.frame(hum,ulna,fem,tib,tar)
logged.vol <- log2(bone.volumes)
scaled.norm.vol <- as.data.frame(scale(logged.vol))
```
  
```{r, echo=F}
library(ggfortify)
library(cluster)

#autoplot(kmeans(logged[,1:10], 6), data = logged , colour = "type") + ggtitle("Bone lenght and diameter - kmeans coloured by attributes")

autoplot(prcomp(logged[,1:10],6), data = logged, colour = "type", frame = T, frame.type = "norm") + ggtitle("Bone lenght and diameter - PCA coloured by attribute")

#autoplot(pam(logged[,1:10],2), data = logged, frame = T, frame.type = "norm") + ggtitle("Bone lenght and diameter - PCA two clusters coloured by clusters") 
#autoplot(pam(logged[,1:10],6), data = logged, frame = T, frame.type = "norm") + ggtitle("Bone lenght and diameter - PCA six clusters coloured by clusters") 

#autoplot(pam(bone.volumes,6), data = logged, frame = T, frame.type = "norm", colour = "type")+ ggtitle("Bone volume - coloured by type") 
#autoplot(pam(bone.volumes,6), data = logged, frame = T, frame.type = "norm")+ ggtitle("Bone volume - PCA six clusters coloured by clusters") 

```   
  
## discussion  
Some birds do not have all attributes and are there for removed from the data. However the question is why these measurements are not present. It was assumed that these were simply not measured. For example there are birds that do not have a femur but did have measurements tibiotarsus. Which seams unlikely if the reason for the absence was that the bird did not have those bones. The absence of these measurements could also still hold information if they aren't measured for another reason.  

The width and length of the bones are heavily correlated however combining these attributes together resulted in a correlation matrix that shows far more correlation than before. Therefore it is still up for debate whether or not it is beneficial for the machine learning algorithim to combine these attributes. Calculating the entropy might give further insight.    
The PCA fails to cluster the six living habitats and shows that a lott of them overlap. However the singing birds and swimming seem to be better clustered when taken apart. Because of this it might be more realistic to  expect the machine learning algorithm to distinguish only the singing birds from the swimming bird with this data set.
```{r, echo=F}
cor.data.scaled.vol <- cor(scaled.norm.vol)
ggcorrplot(cor.data.scaled.vol,  ggtheme = ggplot2::theme_gray,hc.order = T,lab = T)  + ggtitle( "correlation matrix of the bones volumes")
```
  
```{r, echo=F}
library(ggfortify)
library(cluster)

# todo only do swimming ant singing birds + legend

#autoplot(kmeans(logged[,1:10], 6), data = logged , colour = "type") + ggtitle("Bone lenght and diameter - kmeans coloured by attributes")
log_SW_SO <- logged[logged$type %in% c("SW","SO"),T] 

autoplot(prcomp(log_SW_SO[,1:10],6), data = log_SW_SO, colour = "type", frame = T, frame.type = "norm") + ggtitle("Bone lenght and diameter - PCA coloured by attribute") 
autoplot(pam(log_SW_SO[,1:10],2), data = log_SW_SO, frame = T, frame.type = "norm") + ggtitle("Bone lenght and diameter - PCA two clusters coloured by clusters") 
#autoplot(pam(logged[,1:10],6), data = logged, frame = T, frame.type = "norm") + ggtitle("Bone lenght and diameter - PCA six clusters coloured by clusters") 

#autoplot(pam(bone.volumes,6), data = logged, frame = T, frame.type = "norm", colour = "type")+ ggtitle("Bone volume - coloured by type") 
#autoplot(pam(bone.volumes,6), data = logged, frame = T, frame.type = "norm")+ ggtitle("Bone volume - PCA six clusters coloured by clusters") 

```  