---
title: "Birds' bones and living habits"
author: "Jimjim Valkema"
date: "9/10/2019"
output:
  pdf_document: default
  html_document: default
---
```{r, echo=F}

df <- read.csv("bird.csv")
# summary(df)

df <- read.csv("bird.csv")
# remove rows with NA's
df <- df[complete.cases(df),]
```   



```{r, echo=F}
# ggplot(data = min_max_norm, aes(x=huml,y=ulnal,colour = factor(type))) + geom_point() + ggtitle( "correlation between Humerus and Ulna")
# ggplot(data = min_max_norm, aes(x=feml,y=tibl,colour = factor(type))) + geom_point() + ggtitle( "correlation between Femur and Tibiotarsus")
# ggplot(data = min_max_norm, aes(x=huml,y=tibl,colour = factor(type))) + geom_point() + ggtitle( "correlation between Humerus and Tibiotarsus")

# print("lenght vs diameter")
# cor(x=min_max_norm$huml, y= min_max_norm$humw)
# cor(x=min_max_norm$ulnal, y= min_max_norm$ulnaw)
# cor(x=min_max_norm$feml, y= min_max_norm$femw)
# cor(x=min_max_norm$tibl, y= min_max_norm$tibw)
# cor(x=min_max_norm$tarl, y= min_max_norm$tarw)

# ggplot(data = scaled_norm, aes(x=feml,y=femw,colour = factor(type))) + geom_point() + ggtitle( "correlation between Femur length and width")
# ggplot(data = scaled_norm, aes(x=tarl,y=tarw,colour = factor(type))) + geom_point() + ggtitle( "correlation between Tarsometatarsus length and width")

```   
## data set  
The data set has the following attributes:  
huml:  Length of Humerus (mm)  
humw:  Diameter of Humerus (mm)  
ulnal: Length of Ulna (mm)  
ulnaw: Diameter of Ulna (mm)  
feml:  Length of Femur (mm)  
femw:  Diameter of Femur (mm)  
tibl:  Length of Tibiotarsus (mm)  
tibw:  Diameter of Tibiotarsus (mm)  
tarl:  Length of Tarsometatarsus (mm)  
tarw:  Diameter of Tarsometatarsus (mm)  

## introduction  
Defining the living habitat of a bird in archaeological context is a complex and time consuming task when presented with an unknown species. However modern tools like classical machine learning algorithms could help speed up this complex task by predicting its habitat by only analysing a few attributes like the width and length of a few bones which is used in this research. Such algorithms can also be used to find species that might be put in the wrong living habitat. This can be found either if the algorithm disagrees or if it's not very certain if it does agree. Of course the algorithm isn't a replacement for the traditional work that is done to identify the living habitat even though it has a high accuracy. However it might be a useful tool in this field.

## materials and methods

### software  
The exploratory data analysis has been done using rmarkdown with the following packages: 
* ggplot: for creating plots    
* gridExtra: further styling plots  
* reshape2: transform data from wide and long formats  
* rlist: appending items in a list  
* ggcorrplot: creating a correlation plot  
* cluster: creating pca clusters   
  
  
Creating the machine learning model has been done in weka.  
  
Development of the wekawrapper has been done in intellij with the java programming language.  
  
### exploratory data analysis   
Some birds do not have all attributes and are therefore removed from the data. However the question is why these measurements are not present. It was assumed that these were simply not measured. For example there are birds that do not have a femur but did have measurements tibiotarsus. Which seems unlikely if the reason for the absence was that the bird did not have those bones. The absence of these measurements could also still hold information if they aren't measured for another reason.     

The width and length of the bones are heavily correlated however combining these attributes together resulted in a correlation matrix that shows far more correlation than before. Therefore it is still up for debate whether or not it is beneficial for the machine learning algorithm to combine these attributes. Calculating the entropy might give further insight.      
The PCA fails to cluster the six living habitats and shows that a lott of them overlap. However the singing birds and swimming seem to be better clustered when taken apart. Because of this it might be more realistic to expect the machine learning algorithm to distinguish only the singing birds from the swimming bird with this data set.

### evaluation metrics    
The simple accuracy is a good measurement for evaluating a machine learning algorithm in the context of an archaeological application when predicting in which habitat a bird lived. However false positives should be weighted harder if an algorithm is predicting if a bird lived in a specific habitat or not. Because a false positive means it is not only wrong at the habitat it is trying to predict but also at all other habitats. While a false negative means it is wrong at the predicting habitat but not wrong at every other habitat because it says it is in one of them but not which one.    

### speed and scalability
The speed of the algorithm isn't terribly important because the amount of data that will be tested will likely be small in the case of research in evolutionary biology. A likely scenario is that a researcher needs to know the habitat of one or a few birds that are excavated. And testing a bird of the same species doesn't make a lot of sense because birds of the same species have the same bone structure and habitat which again limits the size of the input data for testing. Therefore speed is far less important than accuracy.   
   
### Exploring algorithms and their parameters   
Initially when exploring the algorithms IBK(nearest neighbour) comes on top. However IBK might not be optimal because a bird might fail to be classified if its attributes exceed the range of the training data set because IBK doesn't actually produce a model. Logistic regression comes in second and therefore will probably be the best option because it does produce a model. Random forest produces results that are slightly worse than logistic regression however it might be worth it to try to combine the two with a meta algorithm ie like stacking and voting. J48 does worse than random forest which makes sense because random forest is meta algorithm of j48   Naive Bayes does it worse then OneR so it is probably not worth it to use it in further analysis.

### meta algorithms  
On j48 the cvparameterselection is used to identify the optimal setting for the max tree depth setting which gave a slight accuracy increase. After that j48 was combined together with random forest and logistic regression in a stacking and voting model. The stacking model was used with random forest as a meta classifier. The stacking model outperformed the voting model and was on par with the accuracy of the nearest neighbor. However, the nearest neighbor has the disadvantage of not creating a model and not being able to identify instances outside the range of the training data. There for the stacking meta algorithm is best suited for this data set.

### volume vs all  
Originally it was thought that the length and width of the bones should be combined into a single parameter called volume because those two parameters were highly correlated. However the results when clustering with PCA and using the machine learning model are far worse so it is concluded that too much information is lost in this and not enough simplicity is gained.

### links:
Java wrapper: https://bitbucket.org/jjvalkema/wekawrapper/src/master/   
Rmarkdown EDA: https://bitbucket.org/jjvalkema/thema_9/src/master/   

## results    
There wasn't much cleanup that needed to be done during the exploratory data analysis. However it was found beneficial to remove five instances who weren't complete. Furthermore the data has been log2 transformed and normalised with scaling. It was concluded that a lot of the data was grouped closely together during the principle component analysis. And it was rather difficult to cluster any other groups than swimming and singing birds.    
 
The resulting machine learning model is a stacking meta algorithm with random forest as a meta classifier that combines a j48 tree, logistic regression model and randomforest. This algorithm was tested with 10-fold cross validation in weka and resulted in a 90% accuracy. It was also concluded that making a costsentive classifier wasn't necessary. 
  
The resulting machine learning model isn't very fast on a large number of instances since it uses a stacking meta classifier with three classifiers which one of them is a random forest classifier. Which is also a type of meta classifier of j48. Combining these algorithms does significantly increases the accuracy but it also adds a lott of computations. However it is concluded that accuracy is far more important than speed because the model would be used to classify just a few instances.  

It is also found that so-called "lazy learners" like nearest neighbor aren't an option for this type of data even though the accuracy seems high. This is because it usually leads to a lott of over fitting since this data could contain birds that are outside of the learning data range. And this classification problem requires a more advanced model.   
 
## discussion   
The model build has a reasonable accuracy however it might be interesting to see this algorithm perform on species with far larger measurements that are related to birds and are also bipedal and winged like certain dinosaurs. It would probably be less accurate but if it retains some accuracy then it could mean that it's model is abstract enough to scale to larger measurements and different ecosystems.  
Further exploration in using neural networks might be useful to get a higher accuracy. However it would require a bigger training dataset.


## Conclusion
The data seems to be reasonably complete apart from a few birds missing some attributes so these birds should be removed from the data. The ranges of different attributes are quite significant so the data needs to be normalised.  
It is found that scaling by the standard deviation would be the most effective method for normalization for a future proof machine learning model.  
The data also benefits from a log2 transformation because it seems to be more normal distributed this way.
 
The data is also heavily correlated especially the width and length of the bones. However combining these in a single parameter has not been proven beneficial for most of the tested machine learning models and for principal components analysis.

The PCA plots show that it would be difficult to identify all birds habitats from this data. However the PCA does show that clustering the swimming and singing birds is doable.  
 
The resulting machine learning model seems fairly accurate with an accuracy of 90%. This means it is likely to be useful for archaeological use cases. This algorithm has also been implemented as a java command line tool that can be used to identify birds from a csv file or from a single bird from the command line directly.
   
## project proposal  
Further improvement of the accuracy of the model could be explored in the high throughput biocomputing minor. A more advanced algorithm like a neural network could be trained to possibly get a higher accuracy. Getting a larger dataset would also be beneficial to prevent overfitting. And it could be possible to explorer more nuanced habitats or other useful attributes for the algorithm to classify when training on new data because the new algorithm could be powerful enough to do so.   
Developing a simple user interface might also be a great improvement if this algorithm is used in archeology. This user Interface could provide a way for the user to provide the data and perhaps a simple way to edit it. Having a graphical representation accompanied with general statistics of the resulting classifications could help users that do not have a deep understanding of machine learning to interpret the output of the algorithm.  
Developing an application for desktop computing on platforms like linux,mac and windows would be most useful since mobile applications would be more work to develop while not gaining much usability given the context of archeology. A web app could be useful however the app benefits from being verifiably open source when running locally. This is because researchers need to know exactly what program they are running in order for their work to be reproducible. 
    
## references  
data source: https://www.kaggle.com/zhangjuefei/birds-bones-and-living-habits

```{r, echo=F, warning=F, message=FALSE}
library("ggplot2")
library(gridExtra)
library(reshape2)
library(rlist)
library(tidyr)

#scale the data
scale_min_max <- function(x){
  x_min = min(x, na.rm = T)
  x_max = max(x, na.rm = T)
  return ((x - x_min) / (x_max - x_min))
}

logged <- as.data.frame(log2(df[,2:11]))
min_max_norm <- as.data.frame(apply(logged, MARGIN = 2, FUN = scale_min_max))
scaled_norm <- as.data.frame(scale(logged))

logged["type"] <- df["type"]
min_max_norm["type"] <- df["type"]
scaled_norm["type"] <- df["type"]

# boxplot
melted.oc <- melt(df[2:11])
melted.log.norm <- melt(scaled_norm[1:10])
melted.scaled.norm <- melt(min_max_norm[1:10])
ggplot(data = melted.oc, aes(x=variable,y=value)) + geom_boxplot(outlier.colour="black", outlier.shape=16,outlier.size=2, notch=T) + ggtitle("all attributes not scaled")
#ggplot(data = melted.log.norm, aes(x=variable,y=value)) + geom_boxplot(outlier.colour="black", outlier.shape=16,outlier.size=2, notch=T) + ggtitle("all attributes 2log and scaled")
```
*Figure 1: All the attributes without scaling normalisation as boxplots shows that the range are uneven especially on length vs width*  


```{r, echo=F}
ggplot(data = melted.scaled.norm, aes(x=variable,y=value)) + geom_boxplot(outlier.colour="black", outlier.shape=16,outlier.size=2, notch=T) + ggtitle("all attributes 2log and min and max normalised")

```
  
*Figure 2: All the attributes with scaling normalisation as boxplots shows a much more even range.*  
  
```{r, echo=F}

#create histogram
melted_scaled_norm <- gather(scaled_norm[1:10])
melted_df <- gather(df[2:11])
ggplot(data=melted_df, aes(value))  + facet_wrap(~key,scales="free") + geom_density(adjust = 0.5)+ ggtitle( "attributes density")
```
*Figure 3: Shows the frequency distribution of all the attributes. Here you can see that the data is skewed which suggest that a log2 transform should be applied.*   
```{r, echo=F}
ggplot(data=melted_scaled_norm, aes(value))  + facet_wrap(~key) + geom_density(adjust = 0.5)+ ggtitle("log transformed attributes density")



```
  
*Figure 4: Shows the frequency distribution of all the attributes when log2 transformed. Here you can see that the data is now more normally distributed which should help with most machine learning algorithms to create a better model.*   
  
  
```{r, echo=F}
library("ggcorrplot")
cor.data.scaled <- cor(scaled_norm[,1:10])
ggcorrplot(cor.data.scaled,  ggtheme = ggplot2::theme_gray,hc.order = T,lab = T) + ggtitle( "correlation matrix of the bones length and width")


hum <- df$huml * df$humw
ulna <- df$ulnal * df$ulnaw
fem <- df$feml * df$femw
tib <- df$tibl * df$tibw
tar <- df$tarl * df$tarw

bone.volumes <- data.frame(hum,ulna,fem,tib,tar)
logged.vol <- log2(bone.volumes)
scaled.norm.vol <- as.data.frame(scale(logged.vol))
```
  
*Figure 5: Shows the correlations between the attributes which do seem quite high which might mean that some attributes do not contain a lot of useful data.*   
  
```{r, echo=F}
library(ggfortify)
library(cluster)

#autoplot(kmeans(logged[,1:10], 6), data = logged , colour = "type") + ggtitle("Bone length and diameter - kmeans coloured by attributes")

autoplot(prcomp(logged[,1:10],6), data = logged, colour = "type", frame = T, frame.type = "norm") + ggtitle("Bone length and diameter - PCA coloured by attribute")

#autoplot(pam(logged[,1:10],2), data = logged, frame = T, frame.type = "norm") + ggtitle("Bone length and diameter - PCA two clusters coloured by clusters") 
#autoplot(pam(logged[,1:10],6), data = logged, frame = T, frame.type = "norm") + ggtitle("Bone length and diameter - PCA six clusters coloured by clusters") 

#autoplot(pam(bone.volumes,6), data = logged, frame = T, frame.type = "norm", colour = "type")+ ggtitle("Bone volume - coloured by type") 
#autoplot(pam(bone.volumes,6), data = logged, frame = T, frame.type = "norm")+ ggtitle("Bone volume - PCA six clusters coloured by clusters") 

```   
  
*Figure 6: Shows the pca on the data which has a lott of overlapping clusters which indicates that those attributes might be difficult to differentiate with a machine learning algorithm.*   
  
```{r, echo=F}
cor.data.scaled.vol <- cor(scaled.norm.vol)
ggcorrplot(cor.data.scaled.vol,  ggtheme = ggplot2::theme_gray,hc.order = T,lab = T)  + ggtitle( "correlation matrix of the bones volumes")
```
  
  
*Figure 7: Shows a correlation plot of the measurements of length and width multiplied together which shows that it only increases the correlations and thus will reduce the amount of information available for the machine learning algorithms to use to build a model.*   
  
```{r, echo=F}
library(ggfortify)
library(cluster)

# todo only do swimming ant singing birds + legend

#autoplot(kmeans(logged[,1:10], 6), data = logged , colour = "type") + ggtitle("Bone length and diameter - kmeans coloured by attributes")
log_SW_SO <- logged[logged$type %in% c("SW","SO"),T] 

autoplot(prcomp(log_SW_SO[,1:10],6), data = log_SW_SO, colour = "type", frame = T, frame.type = "norm") + ggtitle("Bone length and diameter - PCA coloured by attribute") 
```
  
*Figure 8: Shows the pca coloured by type of only the singing and swimming birds which separate more clearly which could mean that those will better differentiated by the machine learning algorithm.* 
  
```{r, echo=F}
autoplot(pam(log_SW_SO[,1:10],2), data = log_SW_SO, frame = T, frame.type = "norm") + ggtitle("Bone length and diameter - PCA two clusters coloured by clusters") 
#autoplot(pam(logged[,1:10],6), data = logged, frame = T, frame.type = "norm") + ggtitle("Bone length and diameter - PCA six clusters coloured by clusters") 

#autoplot(pam(bone.volumes,6), data = logged, frame = T, frame.type = "norm", colour = "type")+ ggtitle("Bone volume - coloured by type") 
#autoplot(pam(bone.volumes,6), data = logged, frame = T, frame.type = "norm")+ ggtitle("Bone volume - PCA six clusters coloured by clusters") 

```  
  
*Figure 8: Shows the pca coloured by cluster which show that the pca cluster is able to correctly identify a fair amount of birds.* 
  


